// ===========================================================================
// Page events
// ===========================================================================
// Handle click on the grid
const uiGrid = document.querySelector('.grid-content');
uiGrid.onclick = e => {
    for(let i = 0 ; i < uiGrid.children.length ; i++){
        if(e.target === uiGrid.children[i]){
            tictactoe.putPiece(i);
            break;
        }
    }
};

/**
 * Module for the tic-tac-toe game.
 */
const tictactoe = function(){
    // =======================================================================
    // Private variables
    // =======================================================================
    /**
     * Indicate current player playing.
     */
    let turn = 0;

    /**
     * Indicate whether a player won the game.
     */
    let isDone = false;
    
    /**
     * Array containing all the players
     */
    let players = [ 
        { char: 'x', image: '../img/x.svg' }, 
        { char: 'o', image: '../img/o.svg' } 
    ];
    
    /**
     * The tic-tac-toe grid
     */
    let grid = [
        [null, null, null],
        [null, null, null],
        [null, null, null],
    ];

    // =======================================================================
    // Private functions
    // =======================================================================
    /**
     * Create and add the X or O image to the interface.
     * @param {HTMLElement} cell 
     */
    let addImage = (cell) => {
        // Create new image with the X or the O
        let img = new Image();
        img.src = players[turn].image;
    
        // Add the image to the interface
        cell.appendChild(img);
    };
    
    /**
     * Return true or false depending on if there is a winner.
     * If there is a winner, it should add the class "winner" to all winning cells.
     */
    let checkWinner = () => {
        /* À compléter ... */
        
    };

    // =======================================================================
    // Public functions
    // =======================================================================
    return {
        /**
         * Put a piece on the board.
         */
        putPiece: index => {
            // Calculate x and y coordinates of the grid from the index
            y = Math.floor(index / 3);
            x = index % 3;

            // Check if there is already a piece in that place or if the game is already done
            if(!grid[y][x] && !isDone){
                // Add piece to grid
                grid[y][x] = players[turn].char;
        
                // Add image to interface
                addImage(uiGrid.children[index]);

                // Check if there is a winner
                isDone = checkWinner();

                // Change turn
                turn = (turn + 1) % 2;
            }
        }
    }
}();
